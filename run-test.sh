#!/bin/sh
# vim: tw=0

TESTDIR=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)
. "${TESTDIR}/config.sh"

# We want to use the pre-existing session bus.
export LAUNCH_DBUS="no"

error=0
"${TESTDIR}"/common/run-aa-test "${TESTDIR}"/tumbler.normal.expected "${TESTDIR}"/tumbler normal || error=$?
"${TESTDIR}"/common/run-aa-test "${TESTDIR}"/tumbler.malicious.expected "${TESTDIR}"/tumbler malicious || error=$?

if [ $error = 0 ]; then
  echo "# apparmor-tumbler: all tests passed"
else
  echo "# apparmor-tumbler: at least one test failed"
fi

exit $error
